import reactiveEventManager.AuthorEventManager;
import reactiveEventManager.BookEventManager;
import reactiveEventManager.MainEventManager;
import repository.AuthorRepository;
import repository.BookRepository;
import service.AuthorService;
import service.MainService;
import web.rest.controller.BookController;
import service.BookService;
import web.rest.controller.MainController;

public class Main {

    public static void main(String[] args) {

        BookRepository bookRepository = new BookRepository();
        AuthorRepository authorRepository = new AuthorRepository();

        BookService bookService = new BookService(bookRepository, authorRepository);
//        AuthorService authorService = new AuthorService();
        MainService mainService = new MainService(bookRepository, authorRepository);

        BookEventManager bookEventManager = new BookEventManager(bookService);
//        AuthorEventManager authorEventManager = new AuthorEventManager();
        MainEventManager mainEventManager = new MainEventManager(mainService);


        new BookController(bookEventManager);
        new MainController(mainEventManager);
    }
}
