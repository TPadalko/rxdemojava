package reactiveEventManager;

import io.reactivex.Observable;
import service.MainService;
import web.rest.requestModel.MainRequestModel;
import web.rest.responseModel.MainResponseModel;

public class MainEventManager extends BaseEventManager<MainRequestModel, MainResponseModel> {

    private MainService mainService;

    public MainEventManager(final MainService mainService) {
        this.mainService = mainService;
    }

    @Override
    protected Observable<MainResponseModel> onEvent(MainRequestModel requestModel) {
        switch (requestModel.getEventType()) {
            case All:
                return mainService.getAll();
            default:
                return null;
        }
    }
}
