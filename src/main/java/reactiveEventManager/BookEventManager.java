package reactiveEventManager;

import io.reactivex.Observable;
import service.BookService;
import web.rest.requestModel.BookRequestModel;
import web.rest.responseModel.BookResponseModel;

public class BookEventManager extends BaseEventManager<BookRequestModel, BookResponseModel> {

    private BookService bookService;

    public BookEventManager(final BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    protected Observable<BookResponseModel> onEvent(BookRequestModel requestModel) {
        switch (requestModel.getEventType()) {
            case ById:
                return bookService.handleById((BookRequestModel.ById) requestModel);
            case All:
                return bookService.handleAll((BookRequestModel.All) requestModel);
            case ByAuthorId:
                return bookService.handleAllByAuthorId((BookRequestModel.ByAuthorId) requestModel);
            default:
                System.out.print("unknown");
                return null;
        }
    }
}
