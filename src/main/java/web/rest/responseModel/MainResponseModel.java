package web.rest.responseModel;

import domain.Author;
import domain.Book;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

public class MainResponseModel extends BaseResponseModel {

    @Getter
    @AllArgsConstructor
    public static class All extends MainResponseModel {
        private List<Book> books;
        private List<Author> authors;
    }
}
