package web.rest.responseModel;

import domain.Book;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BookResponseModel extends BaseResponseModel {

    @Getter
    @Setter
    public static class One extends BookResponseModel {

        public Book book;
    }

    @Getter
    @Setter
    public static class All extends BookResponseModel {

        public List<Book> books;
    }
}
