package web.rest.requestModel;

import lombok.Getter;

@Getter
public class MainRequestModel extends BaseRequestModel {
    public enum EventType {
        All
    }
    public EventType eventType;

    public static class All extends MainRequestModel {

        public All() {
            eventType = EventType.All;
        }
    }
}
