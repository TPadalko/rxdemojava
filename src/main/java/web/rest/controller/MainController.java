package web.rest.controller;

import static spark.Spark.*;

import reactiveEventManager.MainEventManager;
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import web.rest.requestModel.MainRequestModel;

import java.util.HashMap;
import java.util.Map;


public class MainController {

    public MainController(final MainEventManager mainEventManager) {

        get("/", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("name", "Fellow");
            return new ModelAndView(model, "index");
        }, new ThymeleafTemplateEngine());


        get("/all", (req, res) -> {
            mainEventManager.event(new MainRequestModel.All());
            return new ModelAndView(mainEventManager.getViewModel(), "all");
        }, new ThymeleafTemplateEngine());


    }
}
