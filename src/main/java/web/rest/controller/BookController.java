package web.rest.controller;

import domain.Book;
import reactiveEventManager.BookEventManager;
import service.BookService;
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import web.rest.requestModel.BookRequestModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.*;
import static utils.JsonUtil.json;


public class BookController {

    public BookController(final BookEventManager bookEventManager) {

        get("/books", (req, res) -> {
            bookEventManager.event(new BookRequestModel.All(null));
            return new ModelAndView(bookEventManager.getViewModel(), "books");
        }, new ThymeleafTemplateEngine());


        get("/book/:id", (req, res) -> {
            String id = req.params(":id");
//            Book book = bookService.findById(id);
            return null;

        }, json());
    }
}
